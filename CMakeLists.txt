cmake_minimum_required(VERSION 3.25)
project(signals)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS Core REQUIRED)

add_library(signals_common INTERFACE)
target_sources(signals_common
    PRIVATE
        common/protocol.h
)
target_include_directories(signals_common
    INTERFACE
        ${CMAKE_CURRENT_LIST_DIR}
)

add_subdirectory(simula)
add_subdirectory(plotter)