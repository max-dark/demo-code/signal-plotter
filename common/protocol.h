//
// Created by max on 01.10.23.
//

#ifndef SIGNALS_PROTOCOL_H
#define SIGNALS_PROTOCOL_H

#include <cstdint>
#include <array>

namespace protocol
{
using item_type = std::int16_t;
using range_type = std::int32_t;

static constexpr std::size_t frame_size = 4 * 1024u;

using frame_type = std::array<item_type, frame_size>;

constexpr uint16_t port = 12345;
} // namespace protocol

#endif //SIGNALS_PROTOCOL_H
