//
// Created by max on 01.10.23.
//

#include "data_receiver.h"

#include <QThread>
#include <QUdpSocket>
#include <QNetworkDatagram>

#include <common/protocol.h>

struct DataReceiver::Self
{
    explicit Self(DataReceiver* parent)
        : parent{parent}
    {
        worker = new QThread{parent};
        socket = new QUdpSocket;

        socket->moveToThread(worker);
    }
    ~Self()
    {
        worker->quit();
        worker->wait();
        socket->deleteLater();
    }
    DataReceiver* parent;

    QThread* worker = nullptr;
    QUdpSocket* socket = nullptr;
};

DataReceiver::DataReceiver(QObject *parent)
        : QObject(parent)
        , self{new Self{this}}
{
    connectEvents();
}

DataReceiver::~DataReceiver()
{
    stop();
    delete self;
}

void DataReceiver::start()
{
    if (!self->worker->isRunning())
    {
        self->worker->start();
    }
}

void DataReceiver::stop()
{
    self->worker->quit();
    self->worker->wait();
}

void DataReceiver::connectEvents()
{
    connect(self->worker, &QThread::finished
            , this, &DataReceiver::stopReceive);
    connect(self->worker, &QThread::started
            , this, &DataReceiver::startReceive);

    connect(self->socket, &QUdpSocket::readyRead
            , this, &DataReceiver::dataReceive);
}

void DataReceiver::dataReceive()
{
    while (self->socket->hasPendingDatagrams())
    {
        auto datagram = self->socket->receiveDatagram();
        onDatagram(datagram.data());
    }
}

void DataReceiver::startReceive()
{
    self->socket->bind(protocol::port);
}

void DataReceiver::stopReceive()
{
    self->socket->close();
}
