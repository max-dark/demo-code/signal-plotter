//
// Created by max on 01.10.23.
//

#ifndef SIGNALS_DATA_RECEIVER_H
#define SIGNALS_DATA_RECEIVER_H

#include <QObject>
#include <QByteArray>

class DataReceiver: public QObject
{
    Q_OBJECT
public:
    explicit DataReceiver(QObject* parent = nullptr);
    ~DataReceiver() override;
signals:
    void onDatagram(QByteArray data);
public slots:
    void start();
    void stop();
private:
    void connectEvents();

private slots:
    void dataReceive();
    void startReceive();
    void stopReceive();
private:
    struct Self;
    Self* self;
};

#endif //SIGNALS_DATA_RECEIVER_H
