#include <QApplication>
#include "ui/plotterwindow.h"

#include "backend/data_receiver.h"


int main(int argc, char ** argv)
{
    QApplication app{argc, argv};
    DataReceiver receiver;
    PlotterWindow mainView;
    mainView.setReceiver(&receiver);

    mainView.show();
    return app.exec(); // NOLINT(readability-static-accessed-through-instance)
}
