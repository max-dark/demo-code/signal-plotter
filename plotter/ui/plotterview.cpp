//
// Created by max on 26.09.23.
//

#include "plotterview.h"

#include <qcustomplot.h>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE
namespace Ui
{
class PlotterView
{
public:
    void setupUi(::PlotterView * self)
    {
        auto box = new QVBoxLayout{self};
        plot = new QCustomPlot{self};
        box->addWidget(plot);
        data.reset(new QCPGraphDataContainer);
        graph = plot->addGraph();
        graph->setData(data);


        const double y_max = std::numeric_limits<protocol::item_type>::max();
        const double y_min = std::numeric_limits<protocol::item_type>::min();

        plot->xAxis->setRange(-5, 5);
        plot->xAxis->setLabel("x");
        plot->yAxis->setRange(y_min, y_max);
        plot->yAxis->setLabel("y");
    }

    [[nodiscard]]
    double rangeX() const
    {
        auto& r = plot->xAxis->range();
        return r.size();
    }

    [[nodiscard]]
    double minX() const
    {
        auto& r = plot->xAxis->range();
        return r.lower;
    }

    [[nodiscard]]
    double maxX() const
    {
        auto& r = plot->xAxis->range();
        return r.upper;
    }

    [[nodiscard]]
    double minY() const
    {
        auto& r = plot->yAxis->range();
        return r.lower;
    }

    [[nodiscard]]
    double maxY() const
    {
        auto& r = plot->yAxis->range();
        return r.upper;
    }

    [[nodiscard]]
    double dx() const
    {
        return rangeX() / protocol::frame_size;
    }

    [[nodiscard]]
    double rangeY() const
    {
        auto& r = plot->yAxis->range();
        return r.size();
    }

    QCustomPlot* plot = nullptr;
    QCPGraph* graph = nullptr;
    QSharedPointer<QCPGraphDataContainer> data;
};

}
QT_END_NAMESPACE


PlotterView::PlotterView(QWidget *parent)
        :
        QWidget(parent), ui{new Ui::PlotterView}
{
    ui->setupUi(this);
}

PlotterView::~PlotterView()
{
    delete ui;
}

void PlotterView::setData(const protocol::frame_type &frame)
{
    const auto minX = ui->minX();
    const auto maxX = ui->maxX();
    const auto minY = ui->minX();
    const auto step = ui->dx();
    const double val_range = std::numeric_limits<protocol::item_type>::max();
    const auto range = ui->rangeY() / val_range;
    const auto& data = ui->data;


    double x = minX;
    data->clear();
    for (const auto v: frame)
    {
        double y = v;
        data->add(QCPGraphData{x, y});
        x += step;
    }

    ui->plot->replot();
}
