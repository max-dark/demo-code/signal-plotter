//
// Created by max on 26.09.23.
//

#ifndef SIGNALS_PLOTTERVIEW_H
#define SIGNALS_PLOTTERVIEW_H

#include <QWidget>

#include <common/protocol.h>

QT_BEGIN_NAMESPACE
namespace Ui
{
class PlotterView;
}
QT_END_NAMESPACE

class PlotterView : public QWidget
{
Q_OBJECT

public:
    explicit PlotterView(QWidget *parent = nullptr);

    ~PlotterView() override;

    void setData(const protocol::frame_type& frame);
private:
    Ui::PlotterView *ui;
};


#endif //SIGNALS_PLOTTERVIEW_H
