//
// Created by max on 26.09.23.
//

#include "plotterwindow.h"
#include "ui_plotterwindow.h"

#include "plotterview.h"
#include <QPainter>
#include <QMessageBox>
#include <QBuffer>
#include <QLabel>

#include "backend/data_receiver.h"

struct PlotterWindow::Self
{
    explicit Self(PlotterWindow* parent)
        : parent{parent}
    {}

    void build()
    {
        frame_count = new QLabel;
        frame_count->setAlignment(Qt::AlignVCenter | Qt::AlignRight);

        parent->statusBar()->addWidget(frame_count, 1);
        setFrameCount(0);
    }

    void setFrameCount(size_t frames)
    {
        num_frames = frames;
        frame_count->setText(QString::number(num_frames));
    }

    [[nodiscard]]
    size_t frameFount() const {
        return num_frames;
    }

    PlotterWindow* parent;
    QLabel* frame_count = nullptr;
    size_t num_frames = 0;
    DataReceiver* receiver = nullptr;
};


PlotterWindow::PlotterWindow(QWidget *parent)
        : QMainWindow(parent)
        , ui(new ::Ui::PlotterWindow)
        , plotter{new PlotterView}
        , self{new Self{this}}
{
    ui->setupUi(this);

    setCentralWidget(plotter);
    self->build();
    connectEvents();
}

PlotterWindow::~PlotterWindow()
{
    self->receiver->stop();
    delete ui;
}

void PlotterWindow::connectEvents()
{
    connect(ui->actionStart, &QAction::triggered
            , this, &PlotterWindow::onStart);
    connect(ui->actionStop, &QAction::triggered
            , this, &PlotterWindow::onStop);
    connect(ui->actionExit, &QAction::triggered
            , this, &PlotterWindow::onExit);
    connect(ui->actionAbout, &QAction::triggered
            , this, &PlotterWindow::onHelpAbout);

}

void PlotterWindow::onHelpAbout()
{
    QMessageBox::aboutQt(this);
}

void PlotterWindow::onStart()
{
    self->num_frames = 0;
    self->receiver->start();
}

void PlotterWindow::onStop()
{
    self->receiver->stop();
}

void PlotterWindow::onExit()
{
    close();
}

void PlotterWindow::setReceiver(DataReceiver *receiver)
{
    if (nullptr == receiver)
        return;
    if (self->receiver == receiver)
        return;

    if (self->receiver)
        self->receiver->disconnect();

    self->receiver = receiver;
    connect(self->receiver, &DataReceiver::onDatagram
            , this, &PlotterWindow::onNewData);
}

void PlotterWindow::onNewData(QByteArray raw)
{
    QBuffer buff{&raw};
    buff.open(QIODevice::ReadOnly);
    protocol::frame_type frame;
    auto* ptr = reinterpret_cast<char*>(frame.data());
    auto count = qMin<qint64>(frame.size() * sizeof(frame[0]), raw.size());

    buff.read(ptr, count);
    plotter->setData(frame);
    self->setFrameCount(self->frameFount() + 1);
}
