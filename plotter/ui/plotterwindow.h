//
// Created by max on 26.09.23.
//

#ifndef SIGNALS_PLOTTERWINDOW_H
#define SIGNALS_PLOTTERWINDOW_H

#include <QMainWindow>
#include <QByteArray>


QT_BEGIN_NAMESPACE
namespace Ui
{
class PlotterWindow;
}
QT_END_NAMESPACE

class PlotterView;
class DataReceiver;

class PlotterWindow : public QMainWindow
{
Q_OBJECT

public:
    explicit PlotterWindow(QWidget *parent = nullptr);

    ~PlotterWindow() override;

    void setReceiver(DataReceiver* receiver);
private:
    void connectEvents();
private slots:
    void onHelpAbout();
    void onStart();
    void onStop();
    void onExit();

    void onNewData(QByteArray raw);

private:
    Ui::PlotterWindow *ui;
    struct Self;
    Self* self;
    PlotterView* plotter;
};


#endif //SIGNALS_PLOTTERWINDOW_H
