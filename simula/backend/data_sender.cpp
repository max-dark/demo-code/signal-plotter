//
// Created by max on 26.09.23.
//

#include "data_sender.h"
#include "generator.h"

#include <thread>
#include <chrono>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <QHostAddress>
#include <QUdpSocket>

#include <common/protocol.h>

struct DataSender::Self
{
    Self(DataSender* server)
        : parent{server}
    {}

    ~Self()
    {
        stop();
    }

    void generation()
    {
        QUdpSocket socket;

        Generator::frame_type frame;
        const auto data_size = frame.size() * sizeof(Generator::item_type);

        while (running)
        {
            std::unique_lock lock{notify_mtx};
            std::cv_status done = std::cv_status::no_timeout;
            while (done != std::cv_status::timeout)
            {
                done = notify_cv.wait_for(lock, period);
            }
            if (!running.load())
            {
                break;
            }

            generator->generate(frame);
            const auto* data = reinterpret_cast<const char*>(frame.data());
            auto sent = socket.writeDatagram(data, data_size, QHostAddress::Broadcast, protocol::port);

            if (sent < 0)
            {
                //break;
                // todo: handle error
            } else {
                ++count;
                emit parent->countChanged();
            }
        }
    }

    void start()
    {
        stop();
        running.store(true);
        worker = std::thread{[this]{
            generation();
        }};
    }

    void stop()
    {
        running = false;
        if (worker.joinable())
        {
            worker.join();
        }
    }

    void notify()
    {
        notify_cv.notify_one();
    }

    std::thread worker;
    std::atomic_bool running;
    std::mutex notify_mtx;
    std::condition_variable notify_cv;
    std::chrono::microseconds period;
    DataSender* parent = nullptr;
    Generator* generator = nullptr;

    std::atomic<size_t> count = 0;
};

DataSender::DataSender(QObject *parent)
        : QObject(parent)
        , self{new Self{this}}
{
}

DataSender::~DataSender()
{
    delete self;
}

Generator *DataSender::generator() const
{
    return self->generator;
}

void DataSender::setGenerator(Generator *generator)
{
    if (nullptr == generator)
        return;
    if (self->generator == generator)
        return;

    self->generator = generator;
}

void DataSender::runGenerator()
{
    self->start();
}

void DataSender::stopGenerator()
{
    self->stop();
}

void DataSender::setPeriod(int period)
{
    {
        std::unique_lock lock{self->notify_mtx};
        self->period = std::chrono::milliseconds{period};
    }
    self->notify();
}

void DataSender::resetCount()
{
    self->count.store(0);
}

size_t DataSender::currentCount() const
{
    return self->count.load();
}
