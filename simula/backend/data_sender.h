//
// Created by max on 26.09.23.
//

#ifndef SIGNALS_DATA_SENDER_H
#define SIGNALS_DATA_SENDER_H

#include <QObject>

class Generator;

class DataSender: public QObject
{
    Q_OBJECT
public:
    DataSender(QObject* parent = nullptr);
    ~DataSender();

    Generator* generator() const;
    size_t currentCount() const;
signals:
    void countChanged();
public slots:
    void setGenerator(Generator* generator);

    void resetCount();
    void runGenerator();
    void stopGenerator();
    void setPeriod(int period);
private:
    struct Self;
    Self* self;
};


#endif //SIGNALS_DATA_SENDER_H
