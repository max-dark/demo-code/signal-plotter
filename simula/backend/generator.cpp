//
// Created by max on 26.09.23.
//

#include "generator.h"

#include <map>

Generator::~Generator() = default;

GeneratorRepository::~GeneratorRepository() = default;

class Repository: public GeneratorRepository
{
public:
    using storage_type = std::map<name_type, Generator*>;

    bool addGenerator(Generator *generator) override;

    Generator *getGenerator(const name_type &name) const override;

    name_list getNames() const override;

    ~Repository() override;
private:
    storage_type storage;
};

bool Repository::addGenerator(Generator *generator)
{
    if (nullptr == generator)
        return false;

    const auto& [_, ok] = storage.try_emplace(generator->name(), generator);

    return ok;
}

GeneratorRepository::name_list Repository::getNames() const
{
    name_list names;

    names.reserve(storage.size());

    for (const auto& [name, generator]: storage)
    {
        names.emplace_back(generator->name());
    }

    return names;
}

Generator *Repository::getGenerator(const GeneratorRepository::name_type &name) const
{
    auto g = storage.find(name);
    if (g != storage.end())
        return g->second;

    return nullptr;
}

Repository::~Repository()
{
    for (const auto& [_, generator] : storage)
    {
        delete generator;
    }
}

GeneratorRepository &GeneratorRepository::instance()
{
    static Repository repository;
    return repository;
}
