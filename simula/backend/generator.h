//
// Created by max on 26.09.23.
//

#ifndef SIGNALS_GENERATOR_H
#define SIGNALS_GENERATOR_H

#include <cstdint>
#include <cstddef>

#include <array>
#include <vector>
#include <string>

#include <common/protocol.h>

class Generator
{
public:

    using item_type = protocol::item_type;
    using range_type = protocol::range_type;

    using frame_type = protocol::frame_type;

    virtual ~Generator();

    virtual const char* name() const = 0;

    virtual void generate(frame_type& frame) = 0;
};

class GeneratorRepository
{
public:
    using name_type = std::string;
    using name_list = std::vector<name_type>;

    virtual ~GeneratorRepository();

    static GeneratorRepository& instance();

    virtual bool addGenerator(Generator* generator) = 0;

    virtual Generator* getGenerator(const name_type& name) const = 0;

    virtual name_list getNames() const = 0;
};


#endif //SIGNALS_GENERATOR_H
