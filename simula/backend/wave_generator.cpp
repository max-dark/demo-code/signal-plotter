//
// Created by max on 01.10.23.
//

#include "wave_generator.h"

#include <cstdint>
#include <cmath>
#include <limits>

void WaveGenerator::generate(Generator::frame_type &frame)
{
    const double range = std::numeric_limits<item_type>::max();

    const double pi = std::acos(0) * 2;
    const double step = pi * 2.0 / static_cast<double>(frame.size());

    double current = -pi;

    for (auto& item: frame)
    {
        item = static_cast<item_type>(cos(current) * range);
        current += step;
    }
}

const char *WaveGenerator::name() const
{
    return "wave";
}
