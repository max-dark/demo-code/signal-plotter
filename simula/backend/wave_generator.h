//
// Created by max on 01.10.23.
//

#ifndef SIGNALS_WAVE_GENERATOR_H
#define SIGNALS_WAVE_GENERATOR_H

#include <backend/generator.h>

class WaveGenerator: public Generator
{
public:
    const char* name() const override;
    void generate(frame_type &frame) override;
};

#endif //SIGNALS_WAVE_GENERATOR_H
