#include <QApplication>

#include "ui/simulasettings.h"

#include "backend/generator.h"
#include "backend/wave_generator.h"
#include "backend/data_sender.h"

#include <cassert>

int main(int argc, char ** argv)
{
    QApplication app{argc, argv};
    auto& generators = GeneratorRepository::instance();
    auto *wave = new WaveGenerator;

    generators.addGenerator(wave);

    DataSender server;
    SimulaSettings mainView;
    mainView.setGenerators(&generators);
    mainView.setPeriodRange(5, 5000);
    mainView.setServer(&server);

    mainView.show();

    return app.exec(); // NOLINT(readability-static-accessed-through-instance)
}
