//
// Created by max on 26.09.23.
//

// You may need to build the project (run Qt uic code generator) to get "ui_SimulaSettings.h" resolved

#include "simulasettings.h"
#include "ui_simulasettings.h"

#include "backend/generator.h"
#include "backend/data_sender.h"

#include <chrono>
#include <QTimer>

using namespace std::literals;

struct SimulaSettings::Self
{
    GeneratorRepository* repository = nullptr;
    DataSender* server = nullptr;
    QTimer* update = nullptr;
    std::size_t currentCount = 0;
};

SimulaSettings::SimulaSettings(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::SimulaSettings)
    , self{new Self}
{
    ui->setupUi(this);

    self->update = new QTimer{this};
    self->update->setInterval(100ms);

    connect(self->update, &QTimer::timeout
            , this, &SimulaSettings::onUpdate);

    connectEvents();
}

void SimulaSettings::connectEvents() const
{
    connect(this, &SimulaSettings::generatorsChanged
            , this, &SimulaSettings::onGeneratorsChanged);
    connect(this, &SimulaSettings::generatorsChanged
            , this, &SimulaSettings::stopGenerator);

    connect(this->ui->btnRun, &QPushButton::clicked
            , this, &SimulaSettings::runGenerator);
    connect(this->ui->btnStop, &QPushButton::clicked
            , this, &SimulaSettings::stopGenerator);
    connect(this->ui->btnReset, &QPushButton::clicked
            , this, &SimulaSettings::resetCounter);

    connect(this->ui->period, &QSlider::valueChanged
            , this, &SimulaSettings::onPeriodChanged);

    connect(this, &SimulaSettings::serverChanged
            , this, &SimulaSettings::onServerChanged);
}

SimulaSettings::~SimulaSettings()
{
    delete ui;
}

void SimulaSettings::setGenerators(GeneratorRepository *generators)
{
    if (nullptr == generators)
        return;
    if (self->repository == generators)
        return;
    self->repository = generators;
    emit generatorsChanged();
}

GeneratorRepository *SimulaSettings::generators() const
{
    return self->repository;
}

void SimulaSettings::onGeneratorsChanged()
{
    auto signalType = ui->signalType;
    signalType->clear();
    auto types = generators()->getNames();

    for (const auto& type: types)
    {
        signalType->addItem(QString::fromStdString(type));
    }
}

void SimulaSettings::runGenerator()
{
    server()->setGenerator(currentGenerator());
    server()->setPeriod(period());
    server()->runGenerator();
    updateButtons(false);
    self->update->start();
}

void SimulaSettings::stopGenerator()
{
    self->update->stop();
    if (server())
    {
        server()->stopGenerator();
    }
    updateButtons(true);
}

void SimulaSettings::resetCounter()
{
    setCounter(0);
    if (server())
    {
        server()->resetCount();
    }
    emit counterReset();
}

void SimulaSettings::onPeriodChanged(int value)
{
    ui->periodLbl->setText(QString::number(value));
    server()->setPeriod(value);
}

std::size_t SimulaSettings::counter() const
{
    return self->currentCount;
}

void SimulaSettings::setCounter(size_t value)
{
    if (counter() == value)
        return;

    self->currentCount = value;
    ui->currentCount->setText(QString::number(value));
    emit counterChanged();
}

void SimulaSettings::setPeriodRange(int min, int max)
{
    ui->period->setRange(min, max);
}

int SimulaSettings::period() const
{
    return ui->period->value();
}

void SimulaSettings::setServer(DataSender *server)
{
    if (nullptr == server)
        return;
    if (server == self->server)
        return;

    self->server = server;
    emit serverChanged();
}

DataSender *SimulaSettings::server() const
{
    return self->server;
}

void SimulaSettings::onServerChanged()
{
    //
}

void SimulaSettings::updateButtons(bool enable)
{
    ui->signalType->setEnabled(enable);
    ui->btnRun->setEnabled(enable);
    ui->btnStop->setEnabled(!enable);
}

Generator * SimulaSettings::currentGenerator() const
{
    auto signalType = ui->signalType;
    const std::string &type = signalType->currentText().toStdString();

    return generators()->getGenerator(type);
}

void SimulaSettings::onUpdate()
{
    setCounter(server()->currentCount());
}
