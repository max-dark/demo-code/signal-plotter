//
// Created by max on 26.09.23.
//

#ifndef SIGNALS_SIMULASETTINGS_H
#define SIGNALS_SIMULASETTINGS_H

#include <QDialog>
#include "backend/generator.h"


class GeneratorRepository;
class DataSender;

QT_BEGIN_NAMESPACE
namespace Ui
{
class SimulaSettings;
}
QT_END_NAMESPACE

class SimulaSettings : public QDialog
{
Q_OBJECT

public:
    explicit SimulaSettings(QWidget *parent = nullptr);

    void setServer(DataSender* server);
    void setGenerators(GeneratorRepository* generators);

    GeneratorRepository* generators() const;
    DataSender* server() const;
    ~SimulaSettings() override;

    std::size_t counter() const;
    int period() const;

    Generator * currentGenerator() const;
signals:
    void generatorsChanged();
    void counterChanged();
    void counterReset();
    void periodChanged(int value);

    void serverChanged();
public slots:
    void stopGenerator();
    void runGenerator();
    void setCounter(size_t value);
    void setPeriodRange(int min, int max);
    void resetCounter();
private slots:
    void onGeneratorsChanged();
    void onPeriodChanged(int value);
    void onServerChanged();
    void onUpdate();
private:
    void connectEvents() const;
    void updateButtons(bool enable);
private:
    Ui::SimulaSettings *ui = nullptr;
    struct Self;
    Self* self;
};


#endif //SIGNALS_SIMULASETTINGS_H
